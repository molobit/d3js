// from data.js
const parser = d3.timeParse('%m/%d/%Y');
const tableData = data.map(entry => {
    return {...entry, datetime: parser(entry.datetime)};
});

const datetimeInput = d3.select('#datetime');
const filterButton = d3.select('#filter-btn');
const resetButton = d3.select('#reset-btn');
const citySelect = d3.select('#city');
const stateSelect = d3.select('#state');
const shapeSelect = d3.select('#shape');
const durationSelect = d3.select('#duration');

function updateTableData(filteredData) {
    let table = d3.select('#ufo-table');
    table.html('');
    table.append('tbody')
        .selectAll('tr')
        .data(filteredData)
        .join('tr')
        .selectAll('td')
        .data(entry => {
            let formattedEntry = {
                ...entry, datetime: entry.datetime.toDateString()
            };

            return Object.values(formattedEntry);
        })
        .join('td')
        .text(entry => entry);
}

function updateSelect(filteredData, el, entryKey) {
    const filteredEntries = filteredData.map(entry => entry[entryKey]).filter(distinct);
    el.selectAll('option')
        .data(["", ...filteredEntries])
        .join('option')
        .attr('value', entry => entry)
        .text(entry => entry);
}

function distinct(value, index, self) {
    return self.indexOf(value) === index;
}

function isDateFormatValid(dateStr) {
    return dateStr.match(/(\d+)\/\d+\/([1-9][0-9][0-9][0-9])/);
}

function filterData(targetDate, city, state, shape, duration) {
    let filteredData = tableData;

    if (targetDate !== null && targetDate !== undefined) {
        filteredData = filteredData.filter((entry) => {
            return entry.datetime.getTime() == targetDate.getTime();
        });
    }

    if (state !== null && state !== undefined) {
        filteredData = filteredData.filter(entry => {
            return entry.state === state;
        });
    }

    if (city !== null && city !== undefined) {
        filteredData = filteredData.filter(entry => {
            return entry.city === city;
        });
    }

    if (shape !== null && shape !== undefined) {
        filteredData = filteredData.filter(entry => {
            return entry.shape === shape;
        });
    }

    if (duration !== null && duration !== undefined) {
        filteredData = filteredData.filter(entry => {
            return entry.durationMinutes === duration;
        });
    }

    return filteredData;
}

function init() {
    /* configure event handlers*/
    resetButton.on('click', () => {
        d3.event.preventDefault();

        datetimeInput.node().value  = '';
        citySelect.node().value     = '';
        stateSelect.node().value    = '';
        shapeSelect.node().value    = '';
        durationSelect.node().value = '';

        updateTableData(tableData);
        updateSelect(tableData, citySelect, 'city');
        updateSelect(tableData, stateSelect, 'state');
        updateSelect(tableData, shapeSelect, 'shape');
        updateSelect(tableData, durationSelect, 'durationMinutes');
    });

    datetimeInput.on('keyup', () => {
        let dateStr = d3.event.target.value;

        if (isDateFormatValid(dateStr)) {
            let targetDate = new Date(dateStr);

            if (targetDate instanceof Date) {

                let filteredData = filterData(targetDate);
                updateTableData(filteredData);
                updateSelect(filteredData, citySelect, 'city');
                updateSelect(filteredData, stateSelect, 'state');
                updateSelect(filteredData, shapeSelect, 'shape');
                updateSelect(filteredData, durationSelect, 'durationMinutes');
            }
        } else if (dateStr === "") {
            updateTableData(tableData);
        }
    });

    stateSelect.on('change', () => {
        let filteredData = tableData.filter(entry => {
            return entry.state === d3.event.target.value;
        });
        updateSelect(filteredData, citySelect, 'city');
    });

    filterButton.on('click', () => {
        d3.event.preventDefault();

        const dateStr     = datetimeInput.node().value;
        const cityStr     = citySelect.node().value;
        const stateStr    = stateSelect.node().value;
        const shapeStr    = shapeSelect.node().value;
        const durationStr = durationSelect.node().value;

        let targetDate     = null;
        let targetCity     = null;
        let targetState    = null;
        let targetShape    = null;
        let targetDuration = null;

        let filteredData = [];

        if (isDateFormatValid(dateStr)) {
            targetDate = new Date(dateStr);
        }

        if (stateStr !== "") {
            targetState = stateStr;
        }

        if (cityStr !== "") {
            targetCity = cityStr;
        }

        if (shapeStr !== "") {
            targetShape = shapeStr;
        }

        if (durationStr !== "") {
            targetDuration = durationStr;
        }

        filteredData = filterData(targetDate, targetCity, targetState, targetShape, targetDuration);

        updateTableData(filteredData);
    });

    /* load all data */
    updateTableData(tableData);

    updateSelect(tableData, citySelect, 'city');
    updateSelect(tableData, stateSelect, 'state');
    updateSelect(tableData, shapeSelect, 'shape');
    updateSelect(tableData, durationSelect, 'durationMinutes');
}

(function() {
    'use strict';
    init();
})();
